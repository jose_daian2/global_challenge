<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Payment extends Model
{
	protected $fillable = ['payment_date', 'expires_at', 'clp_usd', 'user_id', 'status'];

	protected static function boot()
    {
        parent::boot();

        static::creating(function ($payment) {
            $payment->{$payment->getKeyName()} = (string) Str::uuid();
            $payment->status = 'pending';
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    public function user()
	{
	    return $this->belongsTo('App\Client', 'user_id', 'id');
	}
}
