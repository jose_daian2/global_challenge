<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Client extends Model
{
	protected $fillable = ['name', 'email', 'join_date'];

	protected static function boot()
    {
        parent::boot();

        static::creating(function ($client) {
            $client->{$client->getKeyName()} = (string) Str::uuid();
            $client->join_date = date('Y-m-d');
        });
    }

    public function getIncrementing()
    {
        return false;
    }

    public function getKeyType()
    {
        return 'string';
    }

    public function payments() {
        return $this->hasMany(App\Payment::class);
    }
}
