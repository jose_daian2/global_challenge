<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Payment;

class ProcessPayment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $payment;
    protected $payment_date;

    /**
     * Create a new job instance.
     *
     * @param  Payment  $payment
     * @return void
     */
    public function __construct(Payment $payment, $payment_date)
    {
        $this->payment = $payment->withoutRelations();
        $this->payment_date = $payment_date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $last_payment = Payment::where('payment_date', $this->payment_date)->first();

            $update = false;
            $status = 'paid';

            if(!empty($last_payment)){
                $update = true;
                $clp_usd = $last_payment->clp_usd;
            }else{
                $client = new \GuzzleHttp\Client();
                $request = $client->get('https://mindicador.cl/api/dolar/'.date('d-m-Y', strtotime($this->payment_date)));
                $response = json_decode($request->getBody()->getContents());

                if(isset($response->serie)){
                    \Log::info('response', ['r' => $response]);
                    $update = true;
                    $clp_usd = $response->serie[0]->valor;
                }
            }

            if($update){
                Payment::where('id', $this->payment->id)
                    ->update([
                        'payment_date' => $this->payment_date,
                        'clp_usd' => $clp_usd,
                        'status' => $status,
                    ]);

                \Log::info('payment-job', ['update' => $this->payment->id]);
            }
        } catch (Exception $e) {
            \Log::error('exception-payment-job', ['error' => $e]);
        }
    }
}
