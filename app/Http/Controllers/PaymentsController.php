<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Jobs\ProcessPayment;
use App\Events\PaymentEvent;

class PaymentsController extends Controller
{
    /**
     * List all payments per client or create a new record.
     * @param $client_id
     * @return json
     */
    public function payments(Request $request){
    	try {
    		$result = [];
    		if(isset($request->client) && $request->isMethod('get')){
	    		$result = Payment::where('user_id', $request->client)->get();
    		}

    		if($request->isMethod('post')){
    			$payment = new Payment;
		        $payment->expires_at = $request->expires_at;
                $payment->user_id = $request->user_id;
                $payment->payment_date = null;
		        $payment->clp_usd = null;
		        $payment->save();

                ProcessPayment::dispatch($payment, $request->payment_date);
                event(new PaymentEvent($payment));

                return response()->json($payment);
            }

            return response()->json($result);
    	} catch (Exception $e) {
    		\Log::error('Exception', ['error' => $e]);

    		return response()->json([
    			'error' => true,
    			'message' => 'An error has occurred. Try again later'
    		]);
    	}
    }
}
