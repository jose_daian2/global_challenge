<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class ClientsController extends Controller
{
    /**
     * List all clients.
     *
     * @return json
     */
    public function clients(){
    	try {
    		$result = Client::all();

    		return response()->json($result);
    	} catch (Exception $e) {
    		\Log::error('Exception', ['error' => $e]);

    		return response()->json([
    			'error' => true,
    			'message' => 'An error has occurred. Try again later'
    		]);
    	}
    }

    /**
     * Create new client.
     * @param $request 
     * @return json
     */
    public function create(Request $request){
    	try {
	    	$client = new Client;
	        $client->name = $request->name;
	        $client->email = $request->email;
	        $client->save();

	        $response['error'] = false;
	       	$response['message'] = 'Success!';

    	} catch (Exception $e) {
    		\Log::error('Exception', ['error' => $e]);

    		$response['error'] = true;
	       	$response['message'] = 'An error has occurred. Try again later';
    	}

    	return response()->json($response);
    }
}
