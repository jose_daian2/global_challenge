<?php

namespace App\Listeners;

use App\Events\PaymentEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Payment;

class SendPaymentNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentEvent  $event
     * @return void
     */
    public function handle(PaymentEvent $event)
    {
        $payment = Payment::where('id', $event->payment->id)->first();

        \Mail::to($payment->user->email)->send(new \App\Mail\NewMail($payment->user));
        \Log::info('Sending mail success');
    }
}
