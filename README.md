
# Desafio 2x3 Global


### Installation



```sh
$ cd global_challenge
$ composer install
```

### Endpoints
**1. api/clients**
Request Type: **GET**
Response:

    [
        {
            "id": "ccf9707e-ad2a-477d-a791-a6e0bc08b7e0",
            "name": "Jos� Daian Cabrera Rios",
            "email": "a@a.com",
            "join_date": "2020-05-20",
            "created_at": "2020-05-20 12:25:00",
            "updated_at": "2020-05-20 12:25:00"
        }
    ]
**2. api/clients/create**
Request Type: **POST**
Request Params:

 - name
 - email

Response:

    {
        "error": false,
        "message": "Success!"
    }
**3. api/payments**
Request Type: **GET**
Request Params:

 - client

Response:

    [
	    {
	        "id": "a89ef3f6-f585-4b3a-934c-f2594db8fac1",
	        "payment_date": "2020-05-15",
	        "expires_at": "2020-10-25",
	        "status": "paid",
	        "user_id": "ccf9707e-ad2a-477d-a791-a6e0bc08b7e0",
	        "clp_usd": "822.93",
	        "created_at": "2020-05-20 12:32:13",
	        "updated_at": "2020-05-20 12:32:14"
	    },
	    {
	        "id": "ba2e53b0-79cd-4503-b215-650f34144b87",
	        "payment_date": "2020-05-15",
	        "expires_at": "2020-10-25",
	        "status": "paid",
	        "user_id": "ccf9707e-ad2a-477d-a791-a6e0bc08b7e0",
	        "clp_usd": "822.93",
	        "created_at": "2020-05-20 12:29:38",
	        "updated_at": "2020-05-20 12:29:45"
	      }
    ]
**4. api/payments**
Request Type: **POST**
Request Params:

 - user_id
 - expires_at
 - payment_date

> ***Note***: the format for the *expires_at* and *payment_date* parameters is  **YYYY-mm-dd**

Response:

    {
	    "expires_at": "2020-10-25",
	    "user_id": "ccf9707e-ad2a-477d-a791-a6e0bc08b7e0",
	    "id": "e4b7e477-b601-41b2-b86d-88b45b330390",
	    "status": "pending",
	    "updated_at": "2020-05-20 15:26:43",
	    "created_at": "2020-05-20 15:26:43",
		"payment_date": null,
    	"clp_usd": null
	}

License
----
*by* *josedaian*
MIT


